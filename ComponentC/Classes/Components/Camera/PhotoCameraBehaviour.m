//
//  PhotoCameraBehaviour.m
//  ComponentC
//
//  Created by Luka Fajl on 26/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import "PhotoCameraBehaviour.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface PhotoCameraBehaviour () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, copy) void(^callback)(id object, id result);

@end

@implementation PhotoCameraBehaviour

//+(NSArray *) compatibleClasses
//{
//    return @[UIView.class];
//}

-(void) awake
{
    
}

-(void) dealloc
{
    
}

-(void) showCameraWithHandler:(void(^)(id object, id result))block
{
    if (!block) return;
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        block(self.object, nil); // Maybe give NSError as third parameter?
        return;
    }
    
    self.callback = block;
    self.allowVideoRecording = YES;
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    // Displays a control that allows the user to choose picture or
    // movie capture, if both are available:
    if (self.allowVideoRecording)
    {
        cameraUI.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    cameraUI.allowsEditing = self.allowsEditing;
    cameraUI.delegate = self;
    
    UINavigationController *controller = (UINavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    if (_navigationController)
    {
        controller = _navigationController;
    }
    
    [controller presentViewController:cameraUI animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate

-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"info:%@",info);
    [picker dismissViewControllerAnimated:YES completion:^
     {
         NSString *type = info[UIImagePickerControllerMediaType];
         id result = nil;
         if ([type isEqualToString:(NSString *)kUTTypeMovie])
         {
              result = info[UIImagePickerControllerMediaURL];
         }
         else
         {
             result = info[UIImagePickerControllerOriginalImage];
         }
         
         self.callback(self.object, result);
     }];
}

-(void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^
    {
        self.callback(self.object, nil);
    }];
    
}

@end
