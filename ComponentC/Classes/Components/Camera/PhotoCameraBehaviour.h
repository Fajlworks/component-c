//
//  PhotoCameraBehaviour.h
//  ComponentC
//
//  Created by Luka Fajl on 26/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import "NSBehaviour.h"

@interface PhotoCameraBehaviour : NSBehaviour

//optional
@property (nonatomic, weak) UINavigationController *navigationController;

//extras
@property (nonatomic) BOOL allowsEditing;
@property (nonatomic) BOOL allowVideoRecording;


-(void) showCameraWithHandler:(void(^)(id object, id result))block;

@end
