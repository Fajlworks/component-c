//
//  PlaySoundBehaviour.h
//  ComponentC
//
//  Created by Luka Fajl on 27/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import "NSBehaviour.h"

@interface PlaySoundBehaviour : NSBehaviour

@property (nonatomic, copy) NSString *soundFileName;
@property (nonatomic, copy) NSString *fileExtension;

@property (nonatomic) BOOL stackPlaySounds;

-(void) playSound;

@end
