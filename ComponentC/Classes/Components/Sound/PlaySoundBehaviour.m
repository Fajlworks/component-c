//
//  PlaySoundBehaviour.m
//  ComponentC
//
//  Created by Luka Fajl on 27/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import "PlaySoundBehaviour.h"
#import <AudioToolbox/AudioToolbox.h>

@implementation PlaySoundBehaviour
{
    SystemSoundID _previousSSID;
}

- (void)playSound
{
    NSString *path = [[NSBundle mainBundle]
                            pathForResource:_soundFileName ofType:_fileExtension];
    NSURL *URL = [NSURL fileURLWithPath:path];
    
    if (!self.stackPlaySounds)
    {
        AudioServicesDisposeSystemSoundID(_previousSSID);
    }
    
    SystemSoundID ssID = 0;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)URL, &ssID);
    AudioServicesAddSystemSoundCompletion(ssID, NULL, NULL, (AudioServicesSystemSoundCompletionProc)MyAudioServicesSystemSoundCompletionProc, NULL);
    AudioServicesPlaySystemSound(ssID);
    
    if (!self.stackPlaySounds)
    {
        _previousSSID = ssID;
    }
    //AudioServicesDisposeSystemSoundID(ssID);
}

void MyAudioServicesSystemSoundCompletionProc (SystemSoundID  ssID, void *clientData)
{
    AudioServicesDisposeSystemSoundID(ssID);
}

@end
