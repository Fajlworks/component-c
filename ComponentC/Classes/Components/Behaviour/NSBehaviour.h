//
//  NSBehaviour.h
//  Sunsetters
//
//  Created by Luka Fajl on 1/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBehaviour : NSObject

@property (nonatomic, weak) id object;

-(void) _prepareForUsage;

+(NSArray *) compatibleClasses;
-(void) awake;

@end
