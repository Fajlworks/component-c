//
//  NSBehaviour.m
//  Sunsetters
//
//  Created by Luka Fajl on 1/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import "NSBehaviour.h"

@implementation NSBehaviour

+(NSArray *) compatibleClasses
{
    return nil;
}

-(void) _prepareForUsage
{
    NSArray *compatibleClasses = [[self class] compatibleClasses];
    if (compatibleClasses != nil)
    {
        for (Class aClass in compatibleClasses)
        {
             NSAssert1([self.object isKindOfClass:aClass], @"This component has to be added to %@!",NSStringFromClass(aClass));
        }
    }
    
    [self awake];
}

-(void) awake
{
//    QUESTION: is awake neccessary for each behaviour?
//    NSAssert(NO, @"Override!");
}

@end
