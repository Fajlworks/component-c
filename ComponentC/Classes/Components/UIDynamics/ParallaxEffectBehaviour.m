//
//  ParallaxEffectBehaviour.m
//  ComponentC
//
//  Created by Luka Fajl on 2/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import "ParallaxEffectBehaviour.h"

@implementation ParallaxEffectBehaviour
{
    __weak UIInterpolatingMotionEffect *_horizontalEffect;
    __weak UIInterpolatingMotionEffect *_verticalEffect;
}

+(NSArray *) compatibleClasses
{
    return @[UIView.class];
}

-(void) awake
{
    UIView *view = self.object;
    
    UIInterpolatingMotionEffect *effectX = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    UIInterpolatingMotionEffect *effectY = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    
    UIMotionEffectGroup *group = [[UIMotionEffectGroup alloc] init];
    [group setMotionEffects:@[effectX, effectY]];
    
    [view addMotionEffect:group];
    
    _horizontalEffect = effectX;
    _verticalEffect = effectY;
    
    self.parallaxOffset = UIEdgeInsetsZero;
}

-(void) dealloc
{
    
}

#pragma mark - Setter

-(void) setParallaxOffset:(UIEdgeInsets)parallaxOffset
{
    _parallaxOffset = parallaxOffset;
    
    _horizontalEffect.minimumRelativeValue = @(_parallaxOffset.left);
    _horizontalEffect.maximumRelativeValue = @(_parallaxOffset.right);
    _verticalEffect.minimumRelativeValue = @(_parallaxOffset.top);
    _verticalEffect.maximumRelativeValue = @(_parallaxOffset.bottom);
}

@end
