//
//  ShadowEffectBehaviour.m
//  ComponentC
//
//  Created by Luka Fajl on 2/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import "ShadowEffectBehaviour.h"

@implementation ShadowEffectBehaviour

+(NSArray *) compatibleClasses
{
    return @[UIView.class];
}

-(void) awake
{
    UIView *view = self.object;
    
    view.layer.shadowRadius = 1.0f;
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOpacity = 0.5f;
    
    UIInterpolatingMotionEffect *effectX = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    UIInterpolatingMotionEffect *effectY = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    
    UIInterpolatingMotionEffect *shadowEffectX = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"layer.shadowOffset" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    
    shadowEffectX.minimumRelativeValue = [NSValue valueWithCGSize:CGSizeMake(-10, 0)];
    shadowEffectX.maximumRelativeValue = [NSValue valueWithCGSize:CGSizeMake(10, 0)];
   
    UIInterpolatingMotionEffect *shadowEffectY = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"layer.shadowOffset" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    
    shadowEffectY.minimumRelativeValue = [NSValue valueWithCGSize:CGSizeMake(0, -10)];
    shadowEffectY.maximumRelativeValue = [NSValue valueWithCGSize:CGSizeMake(0, 10)];
    
    UIMotionEffectGroup *group = [[UIMotionEffectGroup alloc] init];
    [group setMotionEffects:@[effectX, effectY, shadowEffectX, shadowEffectY]];
    
    [view addMotionEffect:group];
    
    self.horizontalEffect = effectX;
    self.verticalEffect = effectY;
    self.shadowEffect = shadowEffectX;
}

-(void) dealloc
{
    UIView *view = self.object;
    
    [view removeMotionEffect:view.motionEffects.lastObject];
}

@end
