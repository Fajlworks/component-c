//
//  ParallaxEffectBehaviour.h
//  ComponentC
//
//  Created by Luka Fajl on 2/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import "NSBehaviour.h"

@interface ParallaxEffectBehaviour : NSBehaviour

@property (nonatomic) UIEdgeInsets parallaxOffset;

@end
