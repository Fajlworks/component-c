//
//  PressControlBehaviour.h
//  ComponentC
//
//  Created by Luka Fajl on 26/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import "NSBehaviour.h"

@interface PressControlBehaviour : NSBehaviour

@property (nonatomic, copy) void(^touchHandler)(id object);

@end
