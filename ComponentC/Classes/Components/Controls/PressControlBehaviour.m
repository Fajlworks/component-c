//
//  PressControlBehaviour.m
//  ComponentC
//
//  Created by Luka Fajl on 26/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import "PressControlBehaviour.h"

@implementation PressControlBehaviour
{
    __weak UIControl *_this;
    __weak UITapGestureRecognizer *_recognizer;
}

+(NSArray *) compatibleClasses
{
    return @[UIControl.class, UIView.class];
}

-(void) awake
{
    _this = self.object;
    _this.userInteractionEnabled = YES;
    
    if ([_this isKindOfClass:[UIControl class]])
    {
        [_this addTarget:self action:@selector(pressControl_handleTouch) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pressControl_handleTouch)];
        [_this addGestureRecognizer:recognizer];
        _recognizer = recognizer;
    }
}

-(void) dealloc
{
    if ([_this isKindOfClass:[UIControl class]])
    {
        [_this removeTarget:self action:@selector(pressControl_handleTouch) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        if (_recognizer != nil)
            [_this removeGestureRecognizer:_recognizer];
    }
}

-(void) pressControl_handleTouch
{
    if (self.touchHandler)
    {
        self.touchHandler(self.object);
    }
}

@end
