//
//  NSObject+Fajlworks.h
//  FWFoundation
//
//  Created by Luka on 6/21/13.
//  Copyright (c) 2013 Fajlworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Fajlworks)

#pragma mark - Dynamic properties
-(void) setProperty:(NSString *)propertyNamed assign:(id)object;
-(void) setProperty:(NSString *)propertyNamed retain:(id)object;
-(void) setProperty:(NSString *)propertyNamed copy:(id)object;

-(id) property:(NSString *)propertyNamed;

-(void) updateValueForKey:(NSString *)key block:(dispatch_block_t)block;

@end
