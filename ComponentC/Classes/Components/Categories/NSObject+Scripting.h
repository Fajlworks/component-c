//
//  NSObject+Scripting.h
//  Sunsetters
//
//  Created by Luka Fajl on 1/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSBehaviour.h"

#define AddComponent(__OBJ__, __CLASS__) (__CLASS__ *)[__OBJ__ addComponent:[__CLASS__ class]]
#define GetComponent(__OBJ__, __CLASS__) (__CLASS__ *)[__OBJ__ getComponent:[__CLASS__ class]]
#define GetComponents (__OBJ__, __CLASS__) [__OBJ__ getComponents:[__CLASS__ class]]

@interface NSObject (Scripting)

-(NSBehaviour *) addComponent:(Class)aClass;

-(NSBehaviour *) getComponent:(Class)aClass;
-(NSArray *) getComponents:(Class)aClass;

-(void) removeComponent:(NSBehaviour *)behaviour;

/* 
 Convenience to return self as id
 Couldn't come up with a more suitable name
 If anyone has a better solution please refactor
 */
-(id) script;

@end
