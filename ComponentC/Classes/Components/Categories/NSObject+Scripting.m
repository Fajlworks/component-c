//
//  NSObject+Scripting.m
//  Sunsetters
//
//  Created by Luka Fajl on 1/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import "NSObject+Scripting.h"
#import "NSObject+Fajlworks.h"
#import "JRSwizzle.h"

static NSString *kBehaviourList = @"NSObject.Scripting.BehaviourList";

@implementation NSObject (Scripting)

-(BOOL) conformsToComponentRequirements:(Class)aClass
{
    if (![aClass isSubclassOfClass:[NSBehaviour class]])
    {
        NSLog(@"Provided class is not NSBehaviour!");
        return NO;
    }
    
    BOOL conforms = YES;
    NSArray *classes = [aClass compatibleClasses];
    
    if (classes)
    {
        conforms = NO;
        for (Class _class in classes)
        {
            if ([self.class isSubclassOfClass:_class])
            {
                conforms = YES;
            }
        }
    }
    
    return conforms;
}

-(NSBehaviour *) addComponent:(Class)aClass
{
    NSAssert([self conformsToComponentRequirements:aClass], @"Class does not conform to component requirements!");
    
    NSBehaviour *behaviour = [aClass new];
    behaviour.object = self;
    
    [[self fw_behaviourList] addObject:behaviour];
    
    [behaviour _prepareForUsage];
    
    return behaviour;
}

-(NSBehaviour *) getComponent:(Class)aClass
{
    NSMutableArray *list = [self fw_behaviourList];
    __block NSBehaviour *result = nil;
    [list enumerateObjectsUsingBlock:^(NSBehaviour *obj, NSUInteger idx, BOOL *stop)
    {
        if ([obj isKindOfClass:aClass])
        {
            result = obj;
            *stop = YES;
        }
    }];
    
    return result;
}

-(NSArray *) getComponents:(Class)aClass
{
    NSMutableArray *list = [self fw_behaviourList];

    NSIndexSet *indexSet = [list indexesOfObjectsPassingTest:^BOOL(NSBehaviour *obj, NSUInteger idx, BOOL *stop)
    {
        return [obj isKindOfClass:aClass];
    }];
    
    return [list objectsAtIndexes:indexSet];
}

-(void) removeComponent:(NSBehaviour *)behaviour
{
    [[self fw_behaviourList] removeObject:behaviour];
}

-(id) script
{
    return self;
}

#pragma mark - Internal

-(NSMutableArray *) fw_behaviourList
{
    NSMutableArray *behaviourList = [self property:kBehaviourList];
    if (behaviourList == nil)
    {
        behaviourList = [NSMutableArray new];
        [self setProperty:kBehaviourList retain:behaviourList];
    }
    return behaviourList;
}

#pragma mark - Swizzle

+(void) load
{
    [NSObject jr_swizzleMethod:@selector(respondsToSelector:) withMethod:@selector(fw_swizzle_respondsToSelector:) error:nil];
    [NSObject jr_swizzleMethod:@selector(methodSignatureForSelector:) withMethod:@selector(fw_swizzle_methodSignatureForSelector:) error:nil];
    [NSObject jr_swizzleMethod:@selector(forwardInvocation:) withMethod:@selector(fw_swizzle_forwardInvocation:) error:nil];
}

-(BOOL) fw_swizzle_respondsToSelector:(SEL)aSelector
{
    BOOL responds = [self fw_swizzle_respondsToSelector:aSelector];
    
    if (!responds)
    {
        NSMutableArray *list = [self fw_behaviourList];
        for (NSBehaviour *behaviour in list)
        {
            responds = [behaviour fw_swizzle_respondsToSelector:aSelector];
            break;
        }
    }
    
    return responds;
}

- (NSMethodSignature *)fw_swizzle_methodSignatureForSelector:(SEL)aSelector
{
    NSMethodSignature* signature = [self fw_swizzle_methodSignatureForSelector:aSelector];
    if (!signature)
    {
        NSMutableArray *list = [self fw_behaviourList];
        for (NSBehaviour *behaviour in list)
        {
            if (!signature)
            {
                signature = [behaviour methodSignatureForSelector:aSelector];
            }
        }
       
    }
    return signature;
}

-(void) fw_swizzle_forwardInvocation:(NSInvocation *)anInvocation
{
    NSMutableArray *list = [self fw_behaviourList];
    for (NSBehaviour *behaviour in list)
    {
        if ([behaviour respondsToSelector:anInvocation.selector])
        {
            [anInvocation invokeWithTarget:behaviour];
        }
    }
}

@end
