//
//  NSObject+Fajlworks.m
//  FWFoundation
//
//  Created by Luka on 6/21/13.
//  Copyright (c) 2013 Fajlworks. All rights reserved.
//

#import "NSObject+Fajlworks.h"
#import <objc/runtime.h>

@implementation NSObject (Fajlworks)

#pragma mark - Dynamic properties

static NSMutableDictionary *cachedProperties = nil;

NSString *cachedProperty(NSString *property)
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cachedProperties = [NSMutableDictionary new];
    });
    
    //NOTE: consider performance improvement here in future!
    for (NSString *key in [cachedProperties keyEnumerator])
    {
        if ([key isEqualToString:property])
        {
            return [cachedProperties objectForKey:key];
        }
    }
    
    [cachedProperties setObject:property forKey:property];
    
    return property;
}

-(void) setProperty:(NSString *)propertyNamed assign:(id)object
{
    [self setProperty:propertyNamed object:object associationPolicy:OBJC_ASSOCIATION_ASSIGN];
}

-(void) setProperty:(NSString *)propertyNamed retain:(id)object
{
    [self setProperty:propertyNamed object:object associationPolicy:OBJC_ASSOCIATION_RETAIN_NONATOMIC];
}

-(void) setProperty:(NSString *)propertyNamed copy:(id)object
{
    [self setProperty:propertyNamed object:object associationPolicy:OBJC_ASSOCIATION_COPY_NONATOMIC];
}

-(void) setProperty:(NSString *)propertyNamed object:(id)object associationPolicy:(objc_AssociationPolicy)assPolicy
{
    objc_setAssociatedObject(self, (__bridge const void *)(cachedProperty(propertyNamed)), object, assPolicy);
}

-(id) property:(NSString *)propertyNamed
{
    return objc_getAssociatedObject(self, (__bridge const void *)(cachedProperty(propertyNamed)));
}

-(void) updateValueForKey:(NSString *)key block:(dispatch_block_t)block
{
    [self willChangeValueForKey:key];
    block();
    [self didChangeValueForKey:key];
}

@end
