//
//  MasterViewController.h
//  ComponentC
//
//  Created by Luka Fajl on 25/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;

@end
