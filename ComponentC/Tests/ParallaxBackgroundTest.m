//
//  ParallaxBackgroundTest.m
//  ComponentC
//
//  Created by Luka Fajl on 25/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import "ParallaxBackgroundTest.h"
#import "ParallaxEffectBehaviour.h"

@implementation ParallaxBackgroundTest
{
    __weak UIImageView *_imageView;
}

-(NSString *) cellName
{
    return @"Parallax Background Test";
}

-(void) setupTest:(UIView *)view
{
    _imageView = [BaseTestBehaviour setTestImage:@"Apple" inView:view];
    
    ParallaxEffectBehaviour *behaviour = AddComponent(_imageView, ParallaxEffectBehaviour);
    [behaviour setParallaxOffset:UIEdgeInsetsMake(-30, -30, 30, 30)];
    
    [BaseTestBehaviour setTestImage:@"Kii" inView:view];
}

-(void) cleanupTest
{
    [_imageView removeFromSuperview];
}

@end
