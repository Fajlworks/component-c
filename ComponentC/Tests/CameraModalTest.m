//
//  CameraModalTest.m
//  ComponentC
//
//  Created by Luka Fajl on 26/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import "CameraModalTest.h"
#import "PressControlBehaviour.h"
#import "PhotoCameraBehaviour.h"

@implementation CameraModalTest
{
    __weak UIView *_superview;
}

-(NSString *) cellName
{
    return @"Modal Camera Test";
}

-(void) setupTest:(UIView *)view
{
    _superview = view;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setTitle:@"Camera" forState:UIControlStateNormal];
    [button sizeToFit];
    [BaseTestBehaviour setView:button inView:view];
    
      __weak id this = self;
    
    // we add both components to the button
    AddComponent(button, PressControlBehaviour);
    AddComponent(button, PhotoCameraBehaviour);
    
    // button now responds to setTouchHandler selector from PressControlBehaviour component
    // button.script is exaclty like making: id obj = button, since we need id to call methods from components
    [button.script setTouchHandler:^(id object)
    {
        // passed parameter object is the button
        // button also responds to setTouchHandler selector from PhotoCameraBehaviour component
        [object showCameraWithHandler:^(id object, id result)
        {
            //result is id as the component could return video at one time
            [this handleImage:result];
        }];
    }];
}

-(void) handleImage:(UIImage *)image
{
    if (![image isKindOfClass:[UIImage class]])
    {
        NSLog(@"Handle video here!");
        return;
    }
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    [imageView setTransform:CGAffineTransformMakeScale(0.2f, 0.2f)];
    [BaseTestBehaviour setView:imageView inView:_superview];
}

-(void) cleanupTest
{
    
}

@end
