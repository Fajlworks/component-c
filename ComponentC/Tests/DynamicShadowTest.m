//
//  DynamicShadowTest.m
//  ComponentC
//
//  Created by Luka Fajl on 25/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import "DynamicShadowTest.h"
#import "ShadowEffectBehaviour.h"

@implementation DynamicShadowTest
{
    __weak UIImageView *_imageView;
}

-(NSString *) cellName
{
    return @"Dynamic Shadow Test";
}

-(void) setupTest:(UIView *)view
{
    _imageView = [BaseTestBehaviour setTestImage:@"Kii" inView:view];
   
    AddComponent(_imageView, ShadowEffectBehaviour);
}

-(void) cleanupTest
{
    [_imageView removeFromSuperview];
}

@end
