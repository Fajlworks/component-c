//
//  PlaySoundTest.m
//  ComponentC
//
//  Created by Luka Fajl on 2/6/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import "PlaySoundTest.h"
#import "PressControlBehaviour.h"
#import "PlaySoundBehaviour.h"

@implementation PlaySoundTest

-(NSString *) cellName
{
    return @"Play Sound Test";
}

-(void) setupTest:(UIView *)view
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setTitle:@"Sound" forState:UIControlStateNormal];
    [button sizeToFit];
    [BaseTestBehaviour setView:button inView:view];
    
    // we add both components to the button
    AddComponent(button, PressControlBehaviour);
    
    PlaySoundBehaviour *soundBehaviour = AddComponent(button, PlaySoundBehaviour);
    soundBehaviour.soundFileName = @"coin";
    soundBehaviour.fileExtension = @"caf";
    soundBehaviour.stackPlaySounds = YES;
    
    // button now responds to setTouchHandler selector from PressControlBehaviour component
    // button.script is exaclty like making: id obj = button, since we need id to call methods from components
    [button.script setTouchHandler:^(id object)
     {
         // passed parameter object is the button
         // button also responds to playSound selector from PlaySoundBehaviour component
         [object playSound];
     }];
}
@end
