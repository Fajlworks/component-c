//
//  BaseTestBehaviour.m
//  ComponentC
//
//  Created by Luka Fajl on 25/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import "DynamicShadowTest.h"
#import "ParallaxBackgroundTest.h"
#import "CameraModalTest.h"
#import "PlaySoundTest.h"

@implementation BaseTestBehaviour

+(NSArray *) allTests
{
    return @[
             AddComponent([NSObject new], DynamicShadowTest),
             AddComponent([NSObject new], ParallaxBackgroundTest),
             AddComponent([NSObject new], CameraModalTest),
             AddComponent([NSObject new], PlaySoundTest)
             ];
}

-(NSString *) cellName
{
    return @"Override test name!";
}

-(void) setupTest:(UIView *)view
{
    NSAssert(NO, @"Override pls");
}

-(void) cleanupTest
{
    NSAssert(NO, @"Override pls");
}

#pragma mark - Tests helper method

+(UIView *) setView:(UIView *)centerView inView:(UIView *)view
{
    [centerView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [view addSubview:centerView];
    
    // Center horizontally
    [view addConstraint:[NSLayoutConstraint constraintWithItem:centerView
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:view
                                                     attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0
                                                      constant:0.0]];
    
    // Center vertically
    [view addConstraint:[NSLayoutConstraint constraintWithItem:centerView
                                                     attribute:NSLayoutAttributeCenterY
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:view
                                                     attribute:NSLayoutAttributeCenterY
                                                    multiplier:1.0
                                                      constant:0.0]];
    return centerView;
}

+(UIImageView *) setTestImage:(NSString *)imageName inView:(UIView *)view
{
    UIImage *image = [UIImage imageNamed:imageName];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    
    return (UIImageView *)[BaseTestBehaviour setView:imageView inView:view];
}

@end
