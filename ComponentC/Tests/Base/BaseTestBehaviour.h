//
//  BaseTestBehaviour.h
//  ComponentC
//
//  Created by Luka Fajl on 25/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import "NSBehaviour.h"

@interface BaseTestBehaviour : NSBehaviour

+(NSArray *) allTests;
+(UIView *) setView:(UIView *)centerView inView:(UIView *)view;
+(UIImageView *) setTestImage:(NSString *)imageName inView:(UIView *)view;

-(NSString *) cellName;
-(void) setupTest:(UIView *)view;
-(void) cleanupTest;

@end
