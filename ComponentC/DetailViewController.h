//
//  DetailViewController.h
//  ComponentC
//
//  Created by Luka Fajl on 25/5/14.
//  Copyright (c) 2014 Fajlworks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) id detailItem;

@end
